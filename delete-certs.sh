#!/bin/env bash

SERVER_ARN=$(aws resourcegroupstaggingapi get-resources --tag-filters Key=Name,Values=vpn-server-cert --resource-type-filters acm:certificate | jq -r '.ResourceTagMappingList[0].ResourceARN')
CLIENT_ARN=$(aws resourcegroupstaggingapi get-resources --tag-filters Key=Name,Values=vpn-client-cert --resource-type-filters acm:certificate | jq -r '.ResourceTagMappingList[0].ResourceARN')

aws acm delete-certificate --certificate-arn $SERVER_ARN
aws acm delete-certificate --certificate-arn $CLIENT_ARN