#!/bin/env bash

SERVER_ARN=$(aws acm import-certificate --certificate file://terraform/vpc/certificates/server.crt --certificate-chain file://terraform/vpc/certificates/ca.crt --private-key file://terraform/vpc/certificates/server.key | jq -r '.CertificateArn')

aws acm add-tags-to-certificate --certificate-arn $SERVER_ARN --tags Key=Name,Value=vpn-server-cert

CLIENT_ARN=$(aws acm import-certificate --certificate file://terraform/vpc/certificates/client1.domain.tld.crt --certificate-chain file://terraform/vpc/certificates/ca.crt --private-key file://terraform/vpc/certificates/client1.domain.tld.key | jq -r '.CertificateArn')

aws acm add-tags-to-certificate --certificate-arn $CLIENT_ARN --tags Key=Name,Value=vpn-client-cert