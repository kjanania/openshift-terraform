output "bastion-ip" {
  value = "${data.aws_instance.bastion.private_ip}"
}
