spec:
  template:
    spec:
      containers:
        - name: ${app_name}
          image: ${docker_repo}:${docker_image_version}
