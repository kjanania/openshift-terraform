#!/bin/env bash
set -o xtrace
set -a

yum update -y
yum install -y wget git net-tools bind-utils yum-utils iptables-services bridge-utils \
  bash-completion kexec-tools sos psacct openshift-ansible
yum install -y docker