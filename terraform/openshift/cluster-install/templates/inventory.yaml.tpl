all:
  children:
    OSEv3:
      children:
        nodes:
        masters:
        etcd:
      vars:
        openshift_deployment_type: origin
        openshift_enable_excluders: False
        openshift_cloudprovider_kind: aws
        openshift_clusterid: ${openshift-cluster-name}
        openshift_hosted_manage_registry: true
        openshift_hosted_registry_storage_kind: object
        openshift_hosted_registry_storage_provider: s3
        openshift_hosted_registry_storage_s3_bucket: ${s3-bucket-name}
        openshift_hosted_registry_storage_s3_region: ${region}
        openshift_hosted_registry_storage_s3_chunksize: 26214400
        openshift_hosted_registry_storage_s3_rootdirectory: /registry
        openshift_hosted_registry_pullthrough: true
        openshift_hosted_registry_acceptschema2: true
        openshift_hosted_registry_enforcequota: true
        openshift_hosted_registry_replicas: 3
        openshift_disable_check:
          - memory_availability
        os_firewall_use_firewalld: true
        openshift_master_cluster_hostname: ${openshift-masters-load-balancer}
        ansible_service_broker_install: false
        openshift_web_console_install: false
        template_service_broker_install: false
        openshift_console_install: false
        openshift_cluster_monitoring_operator_install: false
        osm_api_server_args:
          authentication-token-webhook-config-file:
            - /etc/origin/cloudprovider/aws-iam-authenticator/kubeconfig.yaml
        openshift_node_groups:
          - name: node-config-master
            labels:
              - 'node-role.kubernetes.io/master=true'
            edits: []
          - name: node-config-infra
            labels:
              - 'node-role.kubernetes.io/infra=true'
            edits: []
          - name: node-config-compute
            labels:
              - 'node-role.kubernetes.io/compute=true'
            edits: []
          - name: node-config-master-infra
            labels:
              - 'node-role.kubernetes.io/infra=true,node-role.kubernetes.io/master=true'
            edits: []
          - name: node-config-all-in-one
            labels:
              - 'node-role.kubernetes.io/infra=true,node-role.kubernetes.io/master=true,node-role.kubernetes.io/compute=true'
            edits: []
    masters:
      hosts:
    %{ for host in masters ~}
    ${host}:
    %{ endfor }
      vars:
        openshift_node_group_name: node-config-all-in-one
    etcd:
      children:
        masters:
    nodes:
      children:
        masters:

  vars:
    ansible_user: ec2-user
    ansible_become: true