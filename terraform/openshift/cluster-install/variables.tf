# variable "workers-asg-name" {
#   description = "OpenShift Worker nodes AutoScaling Group name."
#   type = string
# }

variable "masters-asg-name" {
  description = "OpenShift Master nodes AutoScaling Group name."
  type = string
}

variable "openshift-cluster-name" {
  type = string
}

variable "bastion-instance-id" {
  description = "Used to assure dependencies between modules"
  type = string
}

variable "ec2-key-location" {
  type = string
}

variable "s3-bucket-name" {
  type = string
}

# variable "ec2-worker-instance-count" {
#   type = number
#   default = 3
# }

variable "ec2-master-instance-count" {
  type = number
  default = 3
}

# variable "worker-instance-ips" {
#   type = list(string)
# }

variable "master-instance-ips" {
  type = list(string)
}

variable "bastion-role-arn" {
  type = string
}

variable "worker-role-arn" {
  type = string
}

variable "openshift-masters-lb" {
  type = string
}
