resource "null_resource" "wait-for-cloud-init-masters" {
  count = "${var.ec2-master-instance-count}"
  connection {
    type = "ssh"
    user = "ec2-user"
    host = "${var.master-instance-ips[count.index]}"
    private_key = "${file(var.ec2-key-location)}"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo cloud-init status --wait"
    ]
  }

  triggers = {
    workers = "${join(",", sort(var.master-instance-ips))}"
  }
}


resource "null_resource" "cluster-install" {
  triggers = {
    inventory = "${templatefile("${path.module}/templates/inventory.yaml.tpl", { openshift-cluster-name = "${var.openshift-cluster-name}", masters = var.master-instance-ips, s3-bucket-name = "${var.s3-bucket-name}", region = "${data.aws_region.current.name}", openshift-masters-load-balancer = "${var.openshift-masters-lb}" })}"
  }

  connection {
    type = "ssh"
    user = "ec2-user"
    host = "${data.aws_instance.bastion.private_ip}"
    private_key = "${file(var.ec2-key-location)}"
  }

  provisioner "file" {
    content = "${templatefile("${path.module}/templates/inventory.yaml.tpl", { openshift-cluster-name = "${var.openshift-cluster-name}", masters = var.master-instance-ips, s3-bucket-name = "${var.s3-bucket-name}", region = "${data.aws_region.current.name}", openshift-masters-load-balancer = "${var.openshift-masters-lb}" })}"
    destination = "/home/ec2-user/ansible-hosts.yaml"
  }

  provisioner "file" {
    source = "${path.module}/files/install-cluster.sh"
    destination = "/home/ec2-user/install-cluster.sh"
  }

  provisioner "file" {
    source = "${path.module}/files/fix-masters.yaml"
    destination = "/home/ec2-user/fix-masters.yaml"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/ec2-user/install-cluster.sh",
      "/home/ec2-user/install-cluster.sh"
    ]
  }

  depends_on = ["null_resource.wait-for-cloud-init-masters"]
}

# Run on one master to create the openshift resources
resource "null_resource" "aws-iam-authenticator-install" {
  connection {
    type = "ssh"
    user = "ec2-user"
    host = "${var.master-instance-ips[0]}"
    private_key = "${file(var.ec2-key-location)}"
  }

  provisioner "file" {
    content = "${templatefile("${path.module}/templates/authenticator-deployment.yaml.tpl", { openshift-cluster-name = "${var.openshift-cluster-name}", bastion-role-arn = "${var.bastion-role-arn}", worker-role-arn = "${var.worker-role-arn}"})}"
    destination = "/home/ec2-user/authenticator-deployment.yaml"
  }

  provisioner "remote-exec" {
    inline = [
      "oc patch namespace kube-system -p '{\"metadata\": {\"annotations\": {\"openshift.io/node-selector\": \"\"}}}'",
      "sudo `aws ecr get-login --no-include-email --registry-ids 602401143452 --region ${data.aws_region.current.name}`",
      "sudo cp /root/.docker/config.json ./",
      "sudo chown ec2-user:ec2-user config.json",
      "oc create secret generic aws-pull-secret --from-file=.dockerconfigjson=config.json --type=kubernetes.io/dockerconfigjson -n kube-system",
      "sudo rm -fr /root/.docker/",
      "sudo rm -f config.json",
      "oc apply -f /home/ec2-user/authenticator-deployment.yaml",
      "oc adm policy add-scc-to-user anyuid system:serviceaccount:kube-system:aws-iam-authenticator",
      "oc secrets link aws-iam-authenticator secret/aws-pull-secret -n kube-system --for=pull",
      # Create role binding for auto-approval
      "oc create clusterrolebinding node-client-auto-approve-csr \\",
      "--clusterrole=system:certificates.k8s.io:certificatesigningrequests:nodeclient \\",
      "--group=system:node-bootstrapperkubectl \\",
      "--clusterrole=system:certificates.k8s.io:certificatesigningrequests:selfnodeclient \\",
      "--group=system:nodes",
      "oc create clusterrolebinding node-client-auto-renew-crt \\",
      "--clusterrole=system:certificates.k8s.io:certificatesigningrequests:selfnodeclient \\",
      "--group=system:nodes"
    ]
  }
  depends_on = ["null_resource.cluster-install"]
}

resource "null_resource" "install-complete" {
  depends_on = ["null_resource.cluster-install", "null_resource.aws-iam-authenticator-install"]
}
