data "aws_instance" "bastion" {
  instance_id = "${var.bastion-instance-id}"
}

data "aws_region" "current" {}
