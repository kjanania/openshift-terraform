#!/bin/env bash
set -e
set -o xtrace

git clone https://github.com/openshift/openshift-ansible.git
cd openshift-ansible/
git checkout release-3.11

ansible-playbook -i ../ansible-hosts.yaml playbooks/prerequisites.yml
mv ~/openshift-ansible.log ~/openshift-ansible-pre-req.log

ansible-playbook -i ../ansible-hosts.yaml playbooks/deploy_cluster.yml
mv ~/openshift-ansible.log ~/openshift-deploy-cluster.log

cp ../fix-masters.yaml ./

ansible-playbook -i ../ansible-hosts.yaml fix-masters.yaml

cd
rm -fr openshift-ansible
echo "Completed cluster install"
