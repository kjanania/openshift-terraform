resource "aws_iam_role" "openshift-workers" {
  name = "${var.openshift-cluster-name}-workers"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "openshift-workers" {
  name        = "${var.openshift-cluster-name}-workers"
  path        = "/"
  description = "Policy permissions to allow cluster to manage EC2 instances, load balancers, etc."

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ec2:DescribeInstances"
            ],
            "Resource": "*",
            "Effect": "Allow",
            "Sid": "1"
        },
        {
          "Effect": "Allow",
          "Action": [
            "s3:ListBucket",
            "s3:GetBucketLocation",
            "s3:ListBucketMultipartUploads"
          ],
          "Resource": "arn:aws:s3:::${var.s3-bucket-name}"
        },
        {
          "Effect": "Allow",
          "Action": [
            "s3:PutObject",
            "s3:GetObject",
            "s3:DeleteObject",
            "s3:ListMultipartUploadParts",
            "s3:AbortMultipartUpload"
          ],
          "Resource": "arn:aws:s3:::${var.s3-bucket-name}/*"
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "openshift-workers" {
  name = "${var.openshift-cluster-name}-workers"
  role = "${aws_iam_role.openshift-workers.name}"
}

resource "aws_iam_role_policy_attachment" "openshift-workers-policy-attach" {
  role       = "${aws_iam_role.openshift-workers.name}"
  policy_arn = "${aws_iam_policy.openshift-workers.arn}"
}
