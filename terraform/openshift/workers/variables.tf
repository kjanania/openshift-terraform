variable "subnet-ids" {
  type = list(string)
}

variable "vpc-id" {
  type = string
}

variable "operator-ip" {
  type = string
}

variable "openshift-cluster-name" {
  type = string
}
variable "ec2-key-name" {
  description = "Name of the EC2 SSH key to use for connections."
  type = string
}

variable "ec2-instance-size" {
  description = "Instance size to use for Master nodes in the cluster"
  type = string
  default = "t2.micro"
}

variable "bastion-sg-id" {
  description = "Security group ID used by the bastion host."
  type = string
}

variable "openshift-masters-sg-id" {
  description = "Security group ID used by the OpenShift masters"
  type = string
}

# variable "bastion-instance-id" {
#   description = "Used to assure dependencies between modules"
#   type = string
# }

variable "s3-bucket-name" {
  type = string
}

variable "ec2-instance-count" {
  type = number
}

variable "openshift-masters-lb" {
  type = string
}

variable "cluster-ready" {
  type = string
}
