data "aws_ami" "openshift-rhel-image" {
  filter {
    name   = "name"
    values = ["RHEL-7*GA*"]
  }

  most_recent = true
  owners      = ["309956199498"] # Red Hat AMI Account ID
}

data "aws_region" "current" {}

data "template_file" "userdata" {
  template = "${file("${path.module}/templates/userdata.sh.tpl")}"
  vars = {
    openshift-cluster-name = "${var.openshift-cluster-name}"
    region = "${data.aws_region.current.name}"
    cluster-ready = "${var.cluster-ready}"
    openshift-masters-load-balancer = "${var.openshift-masters-lb}"
  }
}

data "aws_vpc" "vpc" {
  id = "${var.vpc-id}"
}

