output "workers-asg-name" {
  value = "${aws_autoscaling_group.openshift-workers.name}"
}

output "openshift-nodes-sg-id" {
  value = "${aws_security_group.openshift-workers.id}"
}

output "worker-role-arn" {
  value = "${aws_iam_role.openshift-workers.arn}"
}
