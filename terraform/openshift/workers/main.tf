resource "aws_security_group" "openshift-workers" {
  name = "${var.openshift-cluster-name}-workers-security-group"
  description = "Allows the nodes to communicate with the OpenShift cluster"
  vpc_id = "${var.vpc-id}"
  tags = {
    "kubernetes.io/cluster/${var.openshift-cluster-name}" = "owned"
  }
}

resource "aws_security_group_rule" "allow-outbound" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = [
    "0.0.0.0/0"
  ]
  security_group_id = "${aws_security_group.openshift-workers.id}"
}

resource "aws_security_group_rule" "allow-workstation" {
  cidr_blocks = [
    "${data.aws_vpc.vpc.cidr_block}"
  ]
  description       = "Allow workstation to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.openshift-workers.id}"
  to_port           = 443
  type              = "ingress"
}

resource "aws_security_group_rule" "allow-workstation-ssh-workers" {
  cidr_blocks = [
    "${data.aws_vpc.vpc.cidr_block}"
  ]
  description       = "Allow workstation to communicate with the compute nodes"
  from_port         = 22
  protocol          = "tcp"
  security_group_id = "${aws_security_group.openshift-workers.id}"
  to_port           = 22
  type              = "ingress"
}

resource "aws_security_group_rule" "allow-bastion-inbound" {
  type = "ingress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  source_security_group_id = "${var.bastion-sg-id}"
  security_group_id = "${aws_security_group.openshift-workers.id}"
}

resource "aws_security_group_rule" "allow-bastion-outbound" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  source_security_group_id = "${aws_security_group.openshift-workers.id}"
  security_group_id = "${var.bastion-sg-id}"
}

resource "aws_security_group_rule" "allow-masters-inbound-from-worker" {
  type = "ingress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  source_security_group_id = "${aws_security_group.openshift-workers.id}"
  security_group_id = "${var.openshift-masters-sg-id}"
}

resource "aws_security_group_rule" "allow-workers-inbound-from-master" {
  type = "ingress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  source_security_group_id = "${var.openshift-masters-sg-id}"
  security_group_id = "${aws_security_group.openshift-workers.id}"
}

resource "aws_launch_configuration" "openshift-workers" {
  iam_instance_profile        = "${aws_iam_instance_profile.openshift-workers.name}"
  image_id                    = "${data.aws_ami.openshift-rhel-image.id}"
  instance_type               = "${var.ec2-instance-size}"
  name_prefix                 = "${var.openshift-cluster-name}-workers"
  security_groups             = ["${aws_security_group.openshift-workers.id}"]
  user_data_base64            = "${base64encode(data.template_file.userdata.rendered)}"
  key_name = "${var.ec2-key-name}"
  associate_public_ip_address = false
  root_block_device {
    delete_on_termination = true
    volume_size           = 50
    volume_type           = "gp2"
  }


  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "openshift-workers" {
  desired_capacity     = "${var.ec2-instance-count}"
  launch_configuration = "${aws_launch_configuration.openshift-workers.id}"
  max_size             = "${var.ec2-instance-count}"
  min_size             = 1
  name                 = "${var.openshift-cluster-name}-workers"
  vpc_zone_identifier  = var.subnet-ids

  tag {
    key                 = "Name"
    value               = "${var.openshift-cluster-name}-worker"
    propagate_at_launch = true
  }

  tag {
    key = "kubernetes.io/cluster/${var.openshift-cluster-name}"
    value = "owned"
    propagate_at_launch = true
  }

  tag {
    key = "node-role.kubernetes.io/compute"
    value = "true"
    propagate_at_launch = true
  }

  tag {
    key = "clusterid"
    value = "${var.openshift-cluster-name}"
    propagate_at_launch = true
  }

  tag {
    key = "host-type"
    value = "compute"
    propagate_at_launch = true
  }
}

# data "aws_instances" "workers" {
#   filter {
#     name = "tag:aws:autoscaling:groupName"
#     values = ["${aws_autoscaling_group.openshift-nodes.name}"]
#   }
# }

# resource "null_resource" "wait-for-cloud-init-workers" {
#   count = "${var.ec2-instance-count}"
#   connection {
#     type = "ssh"
#     user = "ec2-user"
#     host = "${data.aws_instances.workers.private_ips[count.index]}"
#     private_key = "${file(var.ec2-key-location)}"
#   }
#   provisioner "remote-exec" {
#     inline = [
#       "sudo cloud-init status --wait"
#     ]
#   }

#   triggers = {
#     workers = "${join(",", sort(data.aws_instances.workers.private_ips))}"
#   }
# }