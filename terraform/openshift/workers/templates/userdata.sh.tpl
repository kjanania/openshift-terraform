Content-Type: multipart/mixed; boundary="=="
MIME-Version: 1.0
--==
MIME-Version: 1.0
Content-Type: text/cloud-config; charset="us-ascii"

#cloud-config
bootcmd:
  - cloud-init-per once setup-rhscl-repo yum-config-manager --enable rhui-REGION-rhel-server-rhscl
  - cloud-init-per once setup-extras-repo yum-config-manager --enable rhui-REGION-rhel-server-extras
  - cloud-init-per once add-profile echo -e "#!/bin/bash\nsource /opt/rh/python27/enable" > /etc/profile.d/enablepython27.sh
  - cloud-init-per once create-origin-dir mkdir -p /etc/origin/cloudprovider /etc/origin/node
yum_repos:
  centos-okd:
    baseurl: http://mirror.centos.org/centos/7/paas/x86_64/openshift-origin/
    name: CentOS OpenShift Origin
    enabled: true
    description: OpenShift Origin CentOS Packages
    gpgcheck: 0

package_upgrade: true
packages:
  - git
  - python27-python-pip
  - net-tools
  - bind-utils
  - yum-utils
  - iptables-services
  - bridge-utils
  - bash-completion
  - kexec-tools
  - sos
  - psacct
  - pyOpenSSL
  - python-cryptography
  - python-lxml
  - python27-python-pip
  - unzip
  - device-mapper-persistent-data
  - lvm2
  - java-1.8.0-openjdk-headless
  - patch
  - httpd-tools
  - firewalld
  - container-selinux
write_files:
  - content: |
      [Global]
      Zone = ${region}
    path: /etc/origin/cloudprovider/aws.conf
  - content: |
      all:
        children:
          OSEv3:
            children:
              nodes:
            vars:
          # Add this so the new node can find a master through the load balancer
          masters:
            hosts:
              ${openshift-masters-load-balancer}:
          nodes:
            hosts:
              localhost:
                openshift_node_group_name: node-config-compute
        vars:
          ansible_connection: local
          openshift_deployment_type: origin
          openshift_enable_excluders: False
          openshift_cloudprovider_kind: aws
          openshift_clusterid: ${openshift-cluster-name}
          openshift_disable_check:
            - memory_availability
          os_firewall_use_firewalld: true
          openshift_node_groups:
            - name: node-config-master
              labels:
                - 'node-role.kubernetes.io/master=true'
              edits: []
            - name: node-config-infra
              labels:
                - 'node-role.kubernetes.io/infra=true'
              edits: []
            - name: node-config-compute
              labels:
                - 'node-role.kubernetes.io/compute=true'
              edits: []
            - name: node-config-master-infra
              labels:
                - 'node-role.kubernetes.io/infra=true,node-role.kubernetes.io/master=true'
              edits: []
            - name: node-config-all-in-one
              labels:
                - 'node-role.kubernetes.io/infra=true,node-role.kubernetes.io/master=true,node-role.kubernetes.io/compute=true'
              edits: []
          
    path: /root/ansible-hosts.yaml
  - content: |
      apiVersion: v1
      clusters:
      - cluster:
          certificate-authority-data: $${MASTER_CA_BASE64}
          server: https://${openshift-masters-load-balancer}:8443
        name: ${openshift-masters-load-balancer}:8443
      contexts:
      - context:
          cluster: ${openshift-masters-load-balancer}:8443
          namespace: openshift-infra
          user: node-bootstrapper
        name: node-bootstrapper
      current-context: node-bootstrapper
      kind: Config
      preferences: {}
      users:
      - name: node-bootstrapper
        user:
          exec:
            apiVersion: client.authentication.k8s.io/v1alpha1
            command: aws-iam-authenticator
            args:
              - "token"
              - "-i"
              - "${openshift-cluster-name}"
    path: /etc/origin/node/node.kubeconfig.template

--==
Content-Type: text/x-shellscript; charset="us-ascii"
#!/bin/env bash
set -o xtrace
set -a

echo "Cluster ready resource ID: ${cluster-ready}"

# Install Ansible
curl https://releases.ansible.com/ansible/rpm/release/epel-7-x86_64/ansible-2.8.4-1.el7.ans.noarch.rpm -o ansible-2.8.4-1.el7.ans.noarch.rpm
yum localinstall -y ansible-2.8.4-1.el7.ans.noarch.rpm
rm -f ansible-2.8.4-1.el7.ans.noarch.rpm

# Instal AWS CLI
pip install awscli

curl -o /usr/local/bin/aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/aws-iam-authenticator
chmod +x /usr/local/bin/aws-iam-authenticator

cd /root

git clone https://github.com/openshift/openshift-ansible.git
cd openshift-ansible/
git checkout release-3.11

# Clean cache
rm -rf /var/cache/yum

# Because SELinux blocks firewalld changes during bootstrapping
setenforce Permissive

ansible-playbook -f 1 -i ../ansible-hosts.yaml playbooks/prerequisites.yml
mv ~/openshift-ansible.log ~/openshift-ansible-pre-req.log

ansible-playbook -f 1 -i ../ansible-hosts.yaml playbooks/openshift-node/bootstrap.yml
mv ~/openshift-ansible.log ~/openshift-ansible-bootstrap.log

setenforce Enforcing

openssl s_client -showcerts -connect ${openshift-masters-load-balancer}:8443 < /dev/null 2>/dev/null| sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' | tail -18 > temp.file
export MASTER_CA_BASE64=`base64 temp.file | tr -d '\n'`
rm -f temp.file
envsubst < /etc/origin/node/node.kubeconfig.template > /etc/origin/node/bootstrap.kubeconfig

systemctl start origin-node
--==--
