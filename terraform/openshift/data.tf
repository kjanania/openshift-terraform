data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = "terraform-state-kjanania-redhat"
    key    = "vpc"
    region = "us-east-1"
  }
}

data "http" "operator-ip" {
  url = "http://ipv4.icanhazip.com/"
}

data "aws_instance" "bastion" {
  instance_id = "${module.bastion.bastion-instance-id}"
}
