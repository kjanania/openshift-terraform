resource "aws_iam_role" "openshift-masters" {
  name = "${var.openshift-cluster-name}-masters"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "openshift-masters" {
  name        = "${var.openshift-cluster-name}-masters"
  path        = "/"
  description = "Policy permissions to allow cluster to manage EC2 instances, load balancers, etc."

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ec2:DescribeVolume*",
                "ec2:CreateVolume",
                "ec2:CreateTags",
                "ec2:DescribeInstances",
                "ec2:AttachVolume",
                "ec2:DetachVolume",
                "ec2:DeleteVolume",
                "ec2:DescribeSubnets",
                "ec2:CreateSecurityGroup",
                "ec2:DescribeSecurityGroups",
                "ec2:DescribeRouteTables",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:RevokeSecurityGroupIngress",
                "elasticloadbalancing:DescribeTags",
                "elasticloadbalancing:CreateLoadBalancerListeners",
                "elasticloadbalancing:ConfigureHealthCheck",
                "elasticloadbalancing:DeleteLoadBalancerListeners",
                "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
                "elasticloadbalancing:DescribeLoadBalancers",
                "elasticloadbalancing:CreateLoadBalancer",
                "elasticloadbalancing:DeleteLoadBalancer",
                "elasticloadbalancing:ModifyLoadBalancerAttributes",
                "elasticloadbalancing:DescribeLoadBalancerAttributes"
            ],
            "Resource": "*",
            "Effect": "Allow",
            "Sid": "1"
        },
        {
          "Effect": "Allow",
          "Action": [
            "s3:ListBucket",
            "s3:GetBucketLocation",
            "s3:ListBucketMultipartUploads"
          ],
          "Resource": "arn:aws:s3:::${var.s3-bucket-name}"
        },
        {
          "Effect": "Allow",
          "Action": [
            "s3:PutObject",
            "s3:GetObject",
            "s3:DeleteObject",
            "s3:ListMultipartUploadParts",
            "s3:AbortMultipartUpload"
          ],
          "Resource": "arn:aws:s3:::${var.s3-bucket-name}/*"
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "openshift-masters" {
  name = "${var.openshift-cluster-name}-masters"
  role = "${aws_iam_role.openshift-masters.name}"
}

resource "aws_iam_role_policy_attachment" "openshift-masters-policy-attach" {
  role       = "${aws_iam_role.openshift-masters.name}"
  policy_arn = "${aws_iam_policy.openshift-masters.arn}"
}

resource "aws_iam_role_policy_attachment" "ecr-readonly-policy-attach" {
  role       = "${aws_iam_role.openshift-masters.name}"
  policy_arn = "${data.aws_iam_policy.ecr-readonly.arn}"
}
