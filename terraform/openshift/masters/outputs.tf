output "masters-asg-name" {
  value = "${aws_autoscaling_group.openshift-masters.name}"
}

output "openshift-masters-sg-id" {
  value = "${aws_security_group.openshift-masters.id}"
}

output "master-instance-ips" {
  value = "${data.aws_instances.masters.private_ips}"
}

output "openshift-masters-lb" {
  value = "${aws_alb.openshift-masters-lb.dns_name}"
}
