data "aws_region" "current" {}
data "aws_ami" "openshift-rhel-image" {
  filter {
    name   = "name"
    values = ["RHEL-7*GA*"]
  }

  most_recent = true
  owners      = ["309956199498"] # Red Hat AMI Account ID
}

data "template_file" "userdata" {
  template = "${file("${path.module}/templates/userdata.tpl")}"
  vars = {
    openshift-cluster-name = "${var.openshift-cluster-name}"
    region = "${data.aws_region.current.name}"

    # bastion-instance-id = "${var.bastion-instance-id}"
  }
}

data "aws_instances" "masters" {
  filter {
    name = "tag:aws:autoscaling:groupName"
    values = ["${aws_autoscaling_group.openshift-masters.name}"]
  }
}

data "aws_vpc" "vpc" {
  id = "${var.vpc-id}"
}

data "aws_iam_policy" "ecr-readonly" {
  arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}
