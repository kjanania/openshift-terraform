Content-Type: multipart/mixed; boundary="=="
MIME-Version: 1.0
--==
MIME-Version: 1.0
Content-Type: text/cloud-config; charset="us-ascii"

#cloud-config
bootcmd:
  - cloud-init-per once setup-rhscl-repo yum-config-manager --enable rhui-REGION-rhel-server-rhscl
  - cloud-init-per once setup-extras-repo yum-config-manager --enable rhui-REGION-rhel-server-extras
  - cloud-init-per once add-profile echo -e "#!/bin/bash\nsource /opt/rh/python27/enable" > /etc/profile.d/enablepython27.sh
  - cloud-init-per once create-origin-dir mkdir -p /etc/origin/cloudprovider
yum_repos:
  centos-okd:
    baseurl: http://mirror.centos.org/centos/7/paas/x86_64/openshift-origin/
    name: CentOS OpenShift Origin
    enabled: true
    description: OpenShift Origin CentOS Packages
    gpgcheck: 0

package_upgrade: true
packages:
  - python27-python-pip
  - net-tools
  - bind-utils
  - yum-utils
  - iptables-services
  - bridge-utils
  - bash-completion
  - kexec-tools
  - sos
  - psacct
  - pyOpenSSL
  - python-cryptography
  - python-lxml
  - python27-python-pip
  - unzip
  - device-mapper-persistent-data
  - lvm2
  - java-1.8.0-openjdk-headless
  - patch
  - httpd-tools
  - git
write_files:
  - content: |
      [Global]
      Zone = ${region}
    path: /etc/origin/cloudprovider/aws.conf

--==
Content-Type: text/x-shellscript; charset="us-ascii"
#!/bin/env bash
set -o xtrace
set -a

# Instal AWS CLI
curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
unzip awscli-bundle.zip
./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

curl -o /usr/local/bin/aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/aws-iam-authenticator
chmod +x /usr/local/bin/aws-iam-authenticator

aws-iam-authenticator init -i ${openshift-cluster-name}

mkdir -p /etc/origin/cloudprovider/aws-iam-authenticator/
mkdir -p /var/aws-iam-authenticator/

cp aws-iam-authenticator.kubeconfig /etc/origin/cloudprovider/aws-iam-authenticator/kubeconfig.yaml
cp cert.pem /var/aws-iam-authenticator/cert.pem
cp key.pem /var/aws-iam-authenticator/key.pem

chmod +rw /var/aws-iam-authenticator/*
rm -f aws-iam-authenticator.kubeconfig cert.pem key.pem
--==--
