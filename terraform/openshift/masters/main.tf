resource "aws_security_group" "openshift-masters" {
  name = "${var.openshift-cluster-name}-masters-security-group"
  description = "Allows the masters to communicate with the OpenShift cluster"
  vpc_id = "${var.vpc-id}"
}

resource "aws_security_group_rule" "allow-outbound" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = [
    "0.0.0.0/0"
  ]
  security_group_id = "${aws_security_group.openshift-masters.id}"
}

resource "aws_security_group_rule" "allow-bastion-inbound" {
  type = "ingress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  source_security_group_id = "${var.bastion-sg-id}"
  security_group_id = "${aws_security_group.openshift-masters.id}"
}

resource "aws_security_group_rule" "allow-bastion-outbound" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  source_security_group_id = "${aws_security_group.openshift-masters.id}"
  security_group_id = "${var.bastion-sg-id}"
}


resource "aws_security_group_rule" "allow-workstation" {
  cidr_blocks = [
    "10.0.0.0/16"
  ]
  description       = "Allow workstation to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.openshift-masters.id}"
  to_port           = 443
  type              = "ingress"
}

resource "aws_security_group_rule" "allow-workstation-ssh-masters" {
  cidr_blocks = [
    "10.0.0.0/16"
  ]
  description       = "Allow workstation to communicate with the masters nodes"
  from_port         = 22
  protocol          = "tcp"
  security_group_id = "${aws_security_group.openshift-masters.id}"
  to_port           = 22
  type              = "ingress"
}

resource "aws_security_group_rule" "allow-nlb-health-check" {
  cidr_blocks = [
    "${data.aws_vpc.vpc.cidr_block}"
  ]
  # See for details:
  # https://docs.aws.amazon.com/elasticloadbalancing/latest/network/target-group-register-targets.html#target-security-groups
  description       = "Allow NLB access to perform health check"
  from_port         = 8443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.openshift-masters.id}"
  to_port           = 8443
  type              = "ingress"
}


resource "aws_security_group_rule" "allow-inbound-all-masters" {
  type = "ingress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  source_security_group_id = "${aws_security_group.openshift-masters.id}"
  security_group_id = "${aws_security_group.openshift-masters.id}"  
}


resource "aws_launch_configuration" "openshift-masters" {
  iam_instance_profile        = "${aws_iam_instance_profile.openshift-masters.name}"
  image_id                    = "${data.aws_ami.openshift-rhel-image.id}"
  instance_type               = "${var.ec2-instance-size}"
  name_prefix                 = "${var.openshift-cluster-name}-masters"
  security_groups             = ["${aws_security_group.openshift-masters.id}"]
  user_data_base64            = "${base64encode(data.template_file.userdata.rendered)}"
  key_name = "${var.ec2-key-name}"
  root_block_device {
    delete_on_termination = true
    volume_size           = 50
    volume_type           = "gp2"
  }


  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "openshift-masters" {
  desired_capacity     = "${var.ec2-instance-count}"
  launch_configuration = "${aws_launch_configuration.openshift-masters.id}"
  max_size             = "${var.ec2-instance-count}"
  min_size             = 1
  name                 = "${var.openshift-cluster-name}-masters"
  vpc_zone_identifier  = var.subnet-ids

  target_group_arns = [
    "${aws_lb_target_group.openshift-masters.arn}"
  ]

  tag {
    key                 = "Name"
    value               = "${var.openshift-cluster-name}-master"
    propagate_at_launch = true
  }

  tag {
    key = "kubernetes.io/cluster/${var.openshift-cluster-name}"
    value = "owned"
    propagate_at_launch = true
  }

  tag {
    key = "node-role.kubernetes.io/master"
    value = "true"
    propagate_at_launch = true
  }

  tag {
    key = "clusterid"
    value = "${var.openshift-cluster-name}"
    propagate_at_launch = true
  }

  tag {
    key = "host-type"
    value = "master"
    propagate_at_launch = true
  }
}

resource "aws_alb" "openshift-masters-lb" {
  name               = "${var.openshift-cluster-name}-master"
  internal           = true
  load_balancer_type = "network"
  subnets            = var.subnet-ids


  tags = {
    Name = "openshift-master"
    "kubernetes.io/cluster/${var.openshift-cluster-name}" = "owned"
  }
}

resource "aws_lb_target_group" "openshift-masters" {
  name     = "${var.openshift-cluster-name}-masters"
  port     = 8443
  protocol = "TCP"
  vpc_id   = "${var.vpc-id}"

  health_check {
    path = "/healthz"
    protocol = "HTTPS"
    port = "8443"
  }
}

resource "aws_lb_listener" "openshift-masters" {
  load_balancer_arn = "${aws_alb.openshift-masters-lb.arn}"
  port              = "8443"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.openshift-masters.arn}"
  }
}
