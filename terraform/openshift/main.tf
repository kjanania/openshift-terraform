resource "aws_s3_bucket" "registry-storage" {
  bucket = "${var.openshift-cluster-name}-registry-storage"
  acl    = "private"
}


module "bastion" {
  source = "./bastion"
  ec2-key-name = "${var.ec2-key-name}"
  openshift-cluster-name = "${var.openshift-cluster-name}"
  ec2-instance-size = "${var.ec2-bastion-instance-size}"
  subnet-ids = data.terraform_remote_state.vpc.outputs.private-subnet-ids
  vpc-id = "${data.terraform_remote_state.vpc.outputs.vpc-id}"
  ec2-key-location = "${var.ec2-key-location}"
  s3-bucket-name = "${aws_s3_bucket.registry-storage.id}"
}

module "masters" {
  source = "./masters"
  operator-ip = "${trimspace(data.http.operator-ip.body)}"
  ec2-key-name = "${var.ec2-key-name}"
  openshift-cluster-name = "${var.openshift-cluster-name}"
  ec2-instance-size = "${var.ec2-master-instance-size}"
  subnet-ids = data.terraform_remote_state.vpc.outputs.private-subnet-ids
  vpc-id = "${data.terraform_remote_state.vpc.outputs.vpc-id}"
  bastion-sg-id = "${module.bastion.bastion-sg-id}"
  # bastion-instance-id = "${module.bastion.bastion-instance-id}"
  s3-bucket-name = "${aws_s3_bucket.registry-storage.id}"
  ec2-instance-count = "${var.ec2-master-instance-count}"
}

module "control-plane-install" {
  source = "./cluster-install"
  openshift-cluster-name = "${var.openshift-cluster-name}"
  # workers-asg-name = "${module.workers.workers-asg-name}"
  masters-asg-name = "${module.masters.masters-asg-name}"
  ec2-key-location = "${var.ec2-key-location}"
  bastion-instance-id = "${module.bastion.bastion-instance-id}"
  s3-bucket-name = "${aws_s3_bucket.registry-storage.id}"
  # ec2-worker-instance-count = "${var.ec2-worker-instance-count}"
  ec2-master-instance-count = "${var.ec2-master-instance-count}"
  # worker-instance-ips = "${module.workers.worker-instance-ips}"
  master-instance-ips = "${module.masters.master-instance-ips}"
  bastion-role-arn = "${module.bastion.bastion-role-arn}"
  worker-role-arn = "${module.workers.worker-role-arn}"
  openshift-masters-lb = "${module.masters.openshift-masters-lb}"
}

module "workers" {
  source = "./workers"
  operator-ip = "${trimspace(data.http.operator-ip.body)}"
  ec2-key-name = "${var.ec2-key-name}"
  openshift-cluster-name = "${var.openshift-cluster-name}"
  ec2-instance-size = "${var.ec2-master-instance-size}"
  subnet-ids = data.terraform_remote_state.vpc.outputs.private-subnet-ids
  vpc-id = "${data.terraform_remote_state.vpc.outputs.vpc-id}"
  bastion-sg-id = "${module.bastion.bastion-sg-id}"
  openshift-masters-sg-id = "${module.masters.openshift-masters-sg-id}"
  s3-bucket-name = "${aws_s3_bucket.registry-storage.id}"
  ec2-instance-count = "${var.ec2-worker-instance-count}"
  openshift-masters-lb = "${module.masters.openshift-masters-lb}"
  cluster-ready = "${module.control-plane-install.cluster-ready}"
}



