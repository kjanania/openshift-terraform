terraform {
  backend "s3" {
    key    = "openshift"
    region = "us-east-1"
    dynamodb_table = "terraform-openshift-lock"
  }
}

provider "aws" {
  version = "~> 2.15"
  region = "us-east-1"
}
