variable "openshift-cluster-name" {
  description = "Name of the OpenShift cluster."
  type = string
}
variable "ec2-key-name" {
  description = "Name of the EC2 SSH key to use for connections."
  type = string
}

variable "ec2-worker-instance-size" {
  description = "Instance size to use for Worker nodes in the cluster"
  type = string
  default = "t3.large"
}

variable "ec2-master-instance-size" {
  description = "Instance size to use for Master nodes in the cluster"
  type = string
  default = "t3.large"
}

variable "ec2-bastion-instance-size" {
  description = "Instance size to use for Bastion node in the cluster"
  type = string
  default = "t2.small"
}

variable "ec2-worker-instance-count" {
  description = "The desired size of Worker node ASG."
  type = number
  default = 3
}

variable "ec2-master-instance-count" {
  description = "The desired size of the Master node ASG."
  type = number
  default = 3
}

variable "ec2-key-location" {
  description = "Location of the SSH private key used to launch the EC2 instances."
  type = string
}
