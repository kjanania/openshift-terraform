variable "subnet-ids" {
  type = list(string)
}

variable "vpc-id" {
  type = string
}

variable "openshift-cluster-name" {
  type = string
}
variable "ec2-key-name" {
  description = "Name of the EC2 SSH key to use for connections."
  type = string
}

variable "ec2-instance-size" {
  description = "Instance size to use for Master nodes in the cluster"
  type = string
  default = "t2.small"
}

variable "ec2-key-location" {
  type = string
}

variable "s3-bucket-name" {
  type = string
}

