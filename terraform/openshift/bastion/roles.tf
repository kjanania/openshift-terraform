resource "aws_iam_role" "openshift-bastion" {
  name = "${var.openshift-cluster-name}-bastion"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "openshift-bastion" {
  name        = "${var.openshift-cluster-name}-bastion"
  path        = "/"
  description = "Policy permissions to allow cluster to query EC2 instances, load balancers, etc."

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeAddresses",
                "ec2:DescribeInstances",
                "ec2:DescribeTags",
                "ec2:DescribeInstanceAttribute",
                "ec2:DescribeDhcpOptions",
                "ec2:DescribeSecurityGroups",
                "ec2:DescribeImages",
                "ec2:DescribeAvailabilityZones",
                "ec2:DescribeSecurityGroupReferences",
                "ec2:DescribeImageAttribute",
                "ec2:DescribeSubnets",
                "ec2:DescribeKeyPairs",
                "ec2:DescribeInstanceStatus",
                "ec2:CreateKeyPair"
            ],
            "Resource": "*"
        },
        {
          "Effect": "Allow",
          "Action": [
            "s3:ListBucket",
            "s3:GetBucketLocation",
            "s3:ListBucketMultipartUploads"
          ],
          "Resource": "arn:aws:s3:::${var.s3-bucket-name}"
        },
        {
          "Effect": "Allow",
          "Action": [
            "s3:PutObject",
            "s3:GetObject",
            "s3:DeleteObject",
            "s3:ListMultipartUploadParts",
            "s3:AbortMultipartUpload"
          ],
          "Resource": "arn:aws:s3:::${var.s3-bucket-name}/*"
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "openshift-bastion" {
  name = "${var.openshift-cluster-name}-bastion"
  role = "${aws_iam_role.openshift-bastion.name}"
}

resource "aws_iam_role_policy_attachment" "openshift-masters-policy-attach" {
  role       = "${aws_iam_role.openshift-bastion.name}"
  policy_arn = "${aws_iam_policy.openshift-bastion.arn}"
}
