resource "aws_security_group" "openshift-bastion" {
  name = "${var.openshift-cluster-name}-bastion-security-group"
  description = "Allows the bastion host to communicate with the OpenShift cluster"
  vpc_id = "${var.vpc-id}"
}

resource "aws_security_group_rule" "allow_outbound" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = [
    "0.0.0.0/0"
  ]
  security_group_id = "${aws_security_group.openshift-bastion.id}"
}

resource "aws_security_group_rule" "allow_workstation_ssh-bastion" {
  cidr_blocks = [
    "10.0.0.0/16"
  ]
  description       = "Allow workstation to communicate with the bastion node"
  from_port         = 22
  protocol          = "tcp"
  security_group_id = "${aws_security_group.openshift-bastion.id}"
  to_port           = 22
  type              = "ingress"
}

resource "aws_instance" "bastion" {
  ami           = "${data.aws_ami.openshift-rhel-image.id}"
  instance_type = "${var.ec2-instance-size}"
  subnet_id = "${var.subnet-ids[0]}"
  security_groups = ["${aws_security_group.openshift-bastion.id}"]
  user_data_base64 = "${base64encode(data.template_file.userdata.rendered)}"
  key_name = "${var.ec2-key-name}"
  iam_instance_profile = "${aws_iam_instance_profile.openshift-bastion.name}"
  root_block_device {
    delete_on_termination = true
    volume_size           = 50
    volume_type           = "gp2"
  }

  tags = {
    Name = "${var.openshift-cluster-name}-bastion"
  }

  lifecycle {
    ignore_changes = all
    # ignore_changes = [
    #   "ebs_block_device",
    #   "root_block_device",
    #   "ephemeral_block_device",
    #   "network_interface",
    #   "credit_specification",
    #   "user_data_base64"
    # ] # Required because of an issue with the AWS provider: https://github.com/terraform-providers/terraform-provider-aws/issues/9151
  }

  connection {
    type = "ssh"
    user = "ec2-user"
    host = "${aws_instance.bastion.private_ip}"
    private_key = "${file(var.ec2-key-location)}"
  }

  provisioner "file" {
    source = "${var.ec2-key-location}"
    destination = "/home/ec2-user/.ssh/id_rsa"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo cloud-init status --wait",
      "chmod 600 /home/ec2-user/.ssh/*"
    ]
  }

}
