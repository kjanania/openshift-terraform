output "bastion-instance-id" {
  value = "${aws_instance.bastion.id}"
}

output "bastion-sg-id" {
  value = "${aws_security_group.openshift-bastion.id}"
}

output "bastion-role-arn" {
  value = "${aws_iam_role.openshift-bastion.arn}"
}
