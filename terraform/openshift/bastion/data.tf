data "aws_region" "current" {}

data "aws_ami" "openshift-rhel-image" {
  filter {
    name   = "name"
    values = ["RHEL-7*GA*"]
  }

  most_recent = true
  owners      = ["309956199498"] # Red Hat AMI Account ID
}

data "template_file" "userdata" {
  template = "${file("${path.module}/templates/userdata.tpl")}"
  vars = {
    openshift-cluster-name = "${var.openshift-cluster-name}"
    region = "${data.aws_region.current.name}"
  }
}

data "aws_iam_instance_profile" "bastion-role" {
  name = "aws-ec2-admin"
}
