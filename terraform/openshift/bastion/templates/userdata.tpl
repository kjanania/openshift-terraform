Content-Type: multipart/mixed; boundary="=="
MIME-Version: 1.0
--==
MIME-Version: 1.0
Content-Type: text/cloud-config; charset="us-ascii"

#cloud-config
bootcmd:
  - cloud-init-per once setup-rhscl-repo yum-config-manager --enable rhui-REGION-rhel-server-rhscl
  - cloud-init-per once setup-extras-repo yum-config-manager --enable rhui-REGION-rhel-server-extras
  - cloud-init-per once add-profile echo -e "#!/bin/bash\nsource /opt/rh/python27/enable" > /etc/profile.d/enablepython27.sh
yum_repos:
  centos-okd:
    baseurl: http://mirror.centos.org/centos/7/paas/x86_64/openshift-origin/
    name: CentOS OpenShift Origin
    enabled: true
    description: OpenShift Origin CentOS Packages
    gpgcheck: 0
package_upgrade: true
packages:
  - python27-python-pip
  - git
  - origin-clients

--==
MIME-Version: 1.0
Content-Type: text/x-shellscript; charset="us-ascii"
#!/bin/env bash
set -o xtrace
set -a
source /opt/rh/python27/enable

pip install --upgrade pip
pip install awscli

curl https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 -L -o /usr/local/bin/jq
chmod +x /usr/local/bin/jq
cat <<EOF > /home/ec2-user/.ssh/config
Host 10.0.*
  User ec2-user
  StrictHostKeyChecking no
  UserKnownHostsFile /dev/null
EOF
chown ec2-user:ec2-user /home/ec2-user/.ssh/config
chmod 600 /home/ec2-user/.ssh/config

curl https://releases.ansible.com/ansible/rpm/release/epel-7-x86_64/ansible-2.8.4-1.el7.ans.noarch.rpm -o ansible-2.8.4-1.el7.ans.noarch.rpm
yum localinstall -y ansible-2.8.4-1.el7.ans.noarch.rpm
rm -f ansible-2.8.4-1.el7.ans.noarch.rpm

curl -o /usr/local/bin/aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/aws-iam-authenticator
chmod +x /usr/local/bin/aws-iam-authenticator
--==--