variable "cidr_block" {
  default = "10.0.0.0/16"
}

locals {
  openshift-tags = "${map("kubernetes.io/cluster/${var.openshift-cluster-name}", "shared")}"
}


data "aws_region" "current-region" {}


data "aws_availability_zones" "available" {
    blacklisted_names = ["us-east-1e"]
}


resource "aws_vpc" "main" {
  cidr_block = "${var.cidr_block}"
  enable_dns_hostnames = true
  tags = local.openshift-tags
}

resource "aws_subnet" "private" {
  count = "${length(data.aws_availability_zones.available.names)}"
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block = "${cidrsubnet(var.cidr_block, 8, count.index)}"
  tags = "${local.openshift-tags}"
}


resource "aws_subnet" "public" {
  count = "${length(data.aws_availability_zones.available.names)}"
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block = "${cidrsubnet(var.cidr_block, 8, count.index + length(data.aws_availability_zones.available.names))}"
  map_public_ip_on_launch = true
}

resource "aws_eip" "nat" {
  vpc = true
}


resource "aws_nat_gateway" "nat-gateway" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id = "${aws_subnet.public.*.id[0]}"
  tags = {
      subnet_id = "${aws_subnet.public.*.id[0]}"
      Name = "main-nat-gw"
  }

  depends_on = ["aws_internet_gateway.igw"]
}

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.main.id}"

  tags = {
    Name = "main-igw"
  }
}


resource "aws_route" "default-route-table" {
    route_table_id = "${aws_vpc.main.default_route_table_id}"
    destination_cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.nat-gateway.id}"
}

resource "aws_route_table" "public-route-table" {
    vpc_id = "${aws_vpc.main.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.igw.id}"
    }
}

resource "aws_route_table_association" "public-assoc" {
  count = "${length(data.aws_availability_zones.available.names)}"
  subnet_id      = "${aws_subnet.public.*.id[count.index]}"
  route_table_id = "${aws_route_table.public-route-table.id}"
}
