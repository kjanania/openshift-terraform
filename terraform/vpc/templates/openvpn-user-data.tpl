#!/usr/bin/env bash
set -euxo pipefail
ovpn-init --ec2 --batch --force

echo "openvpn:${password}" | chpasswd

touch /opt/initialized
