data "aws_acm_certificate" "vpn-server-cert" {
  domain      = "server"
  types       = ["IMPORTED"]
  most_recent = true
}


data "aws_acm_certificate" "vpn-client-cert" {
  domain      = "client1.domain.tld"
  types       = ["IMPORTED"]
  most_recent = true
}

# Because of the limited number of certificates you can import by default (20 per region),
# instead of using a Terraform-managed resource, we will use scripts outside of Terraform
# and use data sources.
# resource "aws_acm_certificate" "vpn-server-cert" {
#   private_key      = "${file("${path.module}/certificates/server.key")}"
#   certificate_body = "${file("${path.module}/certificates/server.crt")}"
#   certificate_chain = "${file("${path.module}/certificates/ca.crt")}"
#   tags = {
#       Name = "vpn-server-cert"
#   }
# }

# resource "aws_acm_certificate" "vpn-client-cert" {
#   private_key      = "${file("${path.module}/certificates/client1.domain.tld.key")}"
#   certificate_body = "${file("${path.module}/certificates/client1.domain.tld.crt")}"
#   certificate_chain = "${file("${path.module}/certificates/ca.crt")}"
#   tags = {
#       Name = "vpn-client-cert"
#   }
# }

resource "aws_ec2_client_vpn_endpoint" "solo-vpn" {
  description = "Sets up a VPN for personal use."
  server_certificate_arn = "${data.aws_acm_certificate.vpn-server-cert.arn}"
  client_cidr_block = "${cidrsubnet(var.cidr_block, 6, 2 * length(data.aws_availability_zones.available.names) + 1)}"
  authentication_options {
      type = "certificate-authentication"
      # Ignore the name of this parameter, it actually has nothing to do with the certificate chain.
      # The value should be the ARN of the client certificate.
      root_certificate_chain_arn = "${data.aws_acm_certificate.vpn-client-cert.arn}"
  }
  connection_log_options {
      enabled = false
  }

  provisioner "local-exec" {
    #   command = "aws ec2 export-client-vpn-client-configuration --client-vpn-endpoint-id ${aws_ec2_client_vpn_endpoint.solo-vpn.id} --region ${data.aws_region.current-region.name}"
    command = "mkdir -p ${path.module}/keys && aws ec2 export-client-vpn-client-configuration --client-vpn-endpoint-id ${aws_ec2_client_vpn_endpoint.solo-vpn.id} --region ${data.aws_region.current-region.name} | jq '.ClientConfiguration' | sed 's/\\\\n/\\'$'\\n''/g' | sed 's/\"//g' > ${path.module}/client.config"
  }

  # Required because Terraform AWS provider lacks this feature
  # Add authorization for ingress
  provisioner "local-exec" {
      command = "aws ec2 authorize-client-vpn-ingress --client-vpn-endpoint-id ${aws_ec2_client_vpn_endpoint.solo-vpn.id} --region ${data.aws_region.current-region.name} --target-network-cidr '0.0.0.0/0' --authorize-all-groups"
  }
}

locals {
  sorted-public-subnets = "${sort(aws_subnet.public.*.id)}"
  sorted-private-subnets = "${sort(aws_subnet.private.*.id)}"
  azs = "${length(data.aws_availability_zones.available.names)}"
}


resource "aws_ec2_client_vpn_network_association" "private-subnet" {
  count = "${local.azs > 1 ? 1 : local.azs}"
  client_vpn_endpoint_id = "${aws_ec2_client_vpn_endpoint.solo-vpn.id}"
  subnet_id              = "${element(local.sorted-public-subnets, count.index)}"
  lifecycle {
    ignore_changes = ["subnet_id"] # Required because of an issue with the AWS API: https://github.com/terraform-providers/terraform-provider-aws/issues/7597#issuecomment-472844343
  }

  provisioner "local-exec" {
      command = "aws ec2 create-client-vpn-route --client-vpn-endpoint-id ${aws_ec2_client_vpn_endpoint.solo-vpn.id} --region ${data.aws_region.current-region.name} --destination-cidr-block '0.0.0.0/0' --target-vpc-subnet-id ${element(local.sorted-public-subnets, count.index)}"
  }
}
