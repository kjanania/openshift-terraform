terraform {
  backend "s3" {
    key    = "vpc"
    region = "us-east-1"
    dynamodb_table = "terraform-vpc-lock"
  }
}

provider "aws" {
  version = "~> 2.15"
  region = "us-east-1"
}
