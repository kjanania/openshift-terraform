output "private-subnet-ids" {
  description = "Private subnet IDs for the newly created VPC"
  value = "${aws_subnet.private.*.id}"
}

output "public-subnet-ids" {
  description = "Public subnet IDs for the newly created VPC"
  value = "${aws_subnet.public.*.id}"
}

# output "openvpn-host" {
#   description = "Public DNS name of the OpenVPN host"
#   value = "${aws_instance.openvpn.public_dns}"
# }
output "vpc-id" {
  value = "${aws_vpc.main.id}"
}

output "vpn-dns" {
  value = "${aws_ec2_client_vpn_endpoint.solo-vpn.dns_name}"
}
