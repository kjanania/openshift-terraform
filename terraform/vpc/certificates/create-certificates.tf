# Couldn't get it working perfectly, so just moved it to its own file until I can clean it up.
# CA private key generation
resource "tls_private_key" "ca" {
  algorithm   = "RSA"
  ecdsa_curve = "2048"
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/keys && echo '${tls_private_key.ca.private_key_pem}' > ${path.module}/keys/self-signed-ca-key.pem"
  }

  provisioner "local-exec" {
    when = "destroy"
    command = "rm -fr ${path.module}/keys"
  }
}

# CA for self-signed certification
resource "tls_self_signed_cert" "ca-cert" {
  key_algorithm = "RSA"
  private_key_pem = "${tls_private_key.ca.private_key_pem}"

  subject {
      common_name = "redhat-consulting-examples.com"
      organization = "Red Hat Consulting Examples"
  }

  validity_period_hours = 72
  allowed_uses = [
      "key_encipherment",
      "server_auth",
      "digital_signature"
  ]

  # Store the CA to a file
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/keys && echo '${tls_self_signed_cert.ca-cert.cert_pem}' > ${path.module}/keys/self-signed-ca-cert.pem"
  }

  provisioner "local-exec" {
    when = "destroy"
    command = "rm -fr ${path.module}/keys"
  }
}

# Create server certificate
resource "tls_private_key" "server-cert" {
  algorithm   = "RSA"
  ecdsa_curve = "2048"

  # Store the certificate's private key in a file.
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/keys && echo '${tls_private_key.server-cert.private_key_pem}' > ${path.module}/keys/self-signed-server-key.pem"
  }

  provisioner "local-exec" {
    when = "destroy"
    command = "rm -fr ${path.module}/keys"
  }
}

resource "tls_cert_request" "server-cert" {
  key_algorithm   = "${tls_private_key.server-cert.algorithm}"
  private_key_pem = "${tls_private_key.server-cert.private_key_pem}"

  dns_names    = ["eks.redhat-consulting-examples.com"]


  subject {
      common_name = "redhat-consulting-examples.com"
      organization = "Red Hat Consulting Examples"
  }
}

resource "tls_locally_signed_cert" "server-cert" {
  cert_request_pem = "${tls_cert_request.server-cert.cert_request_pem}"

  ca_key_algorithm   = "${tls_private_key.ca.algorithm}"
  ca_private_key_pem = "${tls_private_key.ca.private_key_pem}"
  ca_cert_pem        = "${tls_self_signed_cert.ca-cert.cert_pem}"

  validity_period_hours = 72
  allowed_uses = [
      "key_encipherment",
      "server_auth",
      "digital_signature"
  ]

  # Store the certificate's public key in a file.
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/keys && echo '${tls_locally_signed_cert.server-cert.cert_pem}' > ${path.module}/keys/self-signed-server-cert.pem"
  }

  provisioner "local-exec" {
    when = "destroy"
    command = "rm -fr ${path.module}/keys"
  }
}

# Create client certificate
resource "tls_private_key" "client-cert" {
  algorithm   = "RSA"
  ecdsa_curve = "2048"

  # Store the certificate's private key in a file.
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/keys && echo '${tls_private_key.client-cert.private_key_pem}' > ${path.module}/keys/self-signed-client-key.pem"
  }

  provisioner "local-exec" {
    when = "destroy"
    command = "rm -fr ${path.module}/keys"
  }
}

resource "tls_cert_request" "client-cert" {
  key_algorithm   = "${tls_private_key.client-cert.algorithm}"
  private_key_pem = "${tls_private_key.client-cert.private_key_pem}"

  dns_names    = ["eks.redhat-consulting-examples.com"]

  subject {
      common_name = "redhat-consulting-examples.com"
      organization = "Red Hat Consulting Examples"
  }
}

resource "tls_locally_signed_cert" "client-cert" {
  cert_request_pem = "${tls_cert_request.client-cert.cert_request_pem}"

  ca_key_algorithm   = "${tls_private_key.ca.algorithm}"
  ca_private_key_pem = "${tls_private_key.ca.private_key_pem}"
  ca_cert_pem        = "${tls_self_signed_cert.ca-cert.cert_pem}"

  validity_period_hours = 72
  allowed_uses = [
      "key_encipherment",
      "server_auth",
      "digital_signature"
  ]

  # Store the certificate's public key in a file.
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/keys && echo '${tls_locally_signed_cert.client-cert.cert_pem}' > ${path.module}/keys/self-signed-client-cert.pem"
  }

  provisioner "local-exec" {
    when = "destroy"
    command = "rm -fr ${path.module}/keys"
  }
}

# resource "aws_acm_certificate" "vpn-server-cert" {
#   private_key      = "${tls_private_key.server-cert.private_key_pem}"
#   certificate_body = "${tls_locally_signed_cert.server-cert.cert_pem}"
#   certificate_chain = "${tls_self_signed_cert.ca-cert.cert_pem}"
# }

# resource "aws_acm_certificate" "vpn-client-cert" {
#   private_key      = "${tls_private_key.client-cert.private_key_pem}"
#   certificate_body = "${tls_locally_signed_cert.client-cert.cert_pem}"
#   certificate_chain = "${tls_self_signed_cert.ca-cert.cert_pem}"
# }
