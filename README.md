# Terraform EKS Plan

## Requirements

* `jq`
* Terraform v0.12.x
* AWS CLI

## Usage
Unfortunately, due to arbitrary limits set on the number of certificates you can upload to ACM, you should use the provided scripts to upload the certificates
outside of Terraform. Otherwise, you can run up against the limit of 20 imports rather quickly.

### Creation
1. Prepare your AWS credentials file
2. `./setup-backend.sh <s3 bucket name>`
3. `./upload-certs.sh`
4. `cd terraform/vpc`
5. `terraform init -backend-config=<s3 bucket name>`
6. `terraform apply -var-file=<path to var file>`
7. In a new terminal: `sudo openvpn --config terraform/vpc/client.config --cert terraform/vpc/certificates/client1.domain.tld.crt --key terraform/vpc/certificates/client1.domain.tld.key`
8. In the original terminal: `cd ../openshift`
9. `terraform init -backend-config=<s3 bucket name>`
10. `terraform apply -var-file=<path to var file>`

### Removal of Resources

1. Prepare your AWS credentials file
2. Stop `openvpn` process
3. `cd terraform/openshift`
4. `terraform destroy`
5. `cd ../terraform/vpc`
6. `terraform destroy`
7. `./destroy-backend.sh <s3 bucket name>`
8. `./delete-certs.sh`

## Variables
To set a specific AWS region, either use an environment variable or set it in `~/.aws/credentials` file.

To override these variables, create a file at the root of the repository with the extension `*.auto.tfvars`. 


### VPC

* `openshift-cluster-name`: Name of the OpenShift cluster. type = string

### OpenShift

* `openshift-cluster-name`: Name of the OpenShift cluster. type = string
* `ec2-key-name`: Name of the EC2 SSH key to use for connections. type = string
* `ec2-worker-instance-size`: Instance size to use for Worker nodes in the cluster. type = string default = "t3.large"
* `ec2-master-instance-size`: Instance size to use for Master nodes in the cluster. type = string default = "t3.large"
* `ec2-bastion-instance-size`: Instance size to use for Bastion node in the cluster. type = string default = "t2.small"
* `ec2-worker-instance-count`: The desired size of Worker node ASG. type = number default = 3
* `ec2-master-instance-count`: The desired size of the Master node ASG. type = number default = 3
* `ec2-key-location`: Location of the SSH private key used to launch the EC2 instances. type = string

## Post Deploy
After deploying this Terraform plan, copy the certificates and client keys found in `vpc/certificates/` to your `~/.cert/` directory. Then you can download and set up a VPN connection using either network manager or manually creating a new OpenVPN connection in RHEL or Fedora. The OpenVPN configuration is downloaded to `vpc/keys/client.config`. This file can be imported to Network Manager with the following command:

```bash
sudo nmcli connection import type openvpn file vpc/keys/client.config`
```
After importing it, you will need to prefix the Gateway DNS with `random.` and make sure that the Authentication is set to `Certificates (TLS)`, and that the proper certificates and keys are configured.

Once that's done, you can connect to the VPN and access your EC2 instances.

## Troubleshooting

  * **I can't access the internet.**  
  Make sure your default security group has outbound access to `0.0.0.0/` on all ports and protocols.
  * **While on VPN, I can access the internet, but it's _really_ slow.**
  If you're on Linux, turning off TCP timestamps may alleviate things:  
  ```bash
  sudo sysctl net.ipv4.tcp_timestamps=0
  ```  
  Otherwise, check the MTU size and make sure it's set to an appropriate size in the OpenVPN configuration.
  * **It doesn't connect to the VPN and I see a SELinux error in `/var/log/messages`**  
  You may need to run `sudo restorecon -R -v ~/.cert` to set the proper context for that directory of certs.
  * **I don't want to use the certificates that are part of the repo**  
  See this [tutorial](https://docs.aws.amazon.com/vpn/latest/clientvpn-admin/authentication-authrization.html#mutual) to create new certificates.