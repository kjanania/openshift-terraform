#!/bin/env bash

aws s3 rb --force s3://${1:-terraform-state-kjanania-redhat}
aws dynamodb delete-table --table-name terraform-vpc-lock
aws dynamodb delete-table --table-name terraform-openshift-lock